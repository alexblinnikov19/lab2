﻿#include <iostream>
#include<vector>
#include<string>
using namespace std;

class Num {

public:
	Num() {}
	class Random {
	public:
		// [min,max]
		static int Get(int min, int max) {
			return  min + (rand() % static_cast<int>(max - min + 1));
		}

	};
	class Karatsuba {
	public:
		static vector<short> AddZero(vector<short>& v, int extend) {
			vector<short> result = vector<short>(v.size() + extend);
			for (int i = 0; i < result.size(); i++)
			{
				if (i < v.size())
					result[i] = v[i];
				else
					result[i] = 0;
			}
			return result;
		}
		static vector<short> GetSub(vector<short>& v, int index, int length) {
			vector<short> result = vector<short>(length);
			for (size_t i = 0; i < length; i++)
			{
				result[i] = v[index + i];
			}
			return result;
		}
		static vector<short> Shift(vector<short>& v, int shift) {
			vector<short> result = vector<short>(shift + v.size());
			for (size_t i = 0; i < shift; i++)
			{
				result[i] = 0;
			}
			for (size_t i = shift; i < result.size(); i++)
			{
				result[i] = v[i - shift];
			}
			return result;
		}
		static vector<short> Int2Vector(int num) {
			auto v = vector<short>(0);
			size_t length = to_string(num).length();
			for (size_t i = 0; i < length; i++)
			{
				v.push_back(num % 10);
				num /= 10;
			}
			return v;
		}
		static vector<short> Multiply(vector<short> v1, vector<short> v2) {
			vector<short> result = vector<short>(0);
			v1 = Num::NonZero(v1);
			v2 = Num::NonZero(v2);
			if (v1.size() == 1 && v2.size() == 1) {
				return Int2Vector(v1[0] * v2[0]);
			}
			vector<short> x1, x0, y1, y0, z2, z1, z0;
			int maxEven = v1.size() > v2.size() ? v1.size() : v2.size();
			if (maxEven % 2 == 1)
				maxEven++;
			v1 = AddZero(v1, maxEven - v1.size());
			v2 = AddZero(v2, maxEven - v2.size());
			x0 = GetSub(v1, 0, maxEven / 2);
			x1 = GetSub(v1, maxEven / 2, maxEven / 2);
			y0 = GetSub(v2, 0, maxEven / 2);
			y1 = GetSub(v2, maxEven / 2, maxEven / 2);

			z2 = Multiply(x1, y1);
			z0 = Multiply(x0, y0);

			z1 = Multiply(Num::SumPosPos(x1, x0), Num::SumPosPos(y1, y0));
			z1 = Num::PositiveSub(z1, z2);
			z1 = Num::PositiveSub(z1, z0);

			result = Shift(z2, maxEven);
			result = Num::SumPosPos(result, Shift(z1, maxEven / 2));
			result = Num::SumPosPos(result, z0);

			return result;
		}
	};
	class Division {
	public:
		static Num Divide(const Num& a, Num& b, Num& remainder) {
			if (CompareAbs(a.v, b.v) == -1) {
				remainder = a;
				return 0;
			}
			string result = "";
			auto aVCopy = a.v;
			int aSize = a.v.size();
			int bSize = b.v.size();
			int lastIndex = aSize - bSize;
			int index;
			auto subVN = [&]()
			{
				vector<short> sub(0);
				for (int i = lastIndex; i < aSize; i++)
				{
					sub.push_back(aVCopy[i]);
				}
				return Num(sub, true);
			};
			while (lastIndex >= 0)
			{
				if (CompareAbs(subVN().v, b.v) == -1) {
					result += '0';
				}
				else {
					index = 0;
					for (int i = 2; i <= 10; i++)
					{
						if (CompareAbs(subVN().v, (b * Num(i)).v) == -1) {
							index = i - 1;
							break;
						}
					}
					if (index == 0) {
						cout << "smth went wrong" << endl;
						exit(1);
					}
					result += to_string(index);
					vector<short> newSubV = (subVN() - b * Num(index)).v;
					for (int i = lastIndex; i < aSize; i++)
					{
						if (i - lastIndex < NonZero(newSubV).size())
							aVCopy[i] = newSubV[i - lastIndex];
						else
							aVCopy[i] = 0;
					}
				}
				lastIndex--;
			}
			remainder = Num(aVCopy, true);
			return Num(NonZero(Num(result).v), true);
		}

	};
	class ToomCook3 {
	public:
		static Num Multiply(Num  n1, Num n2) {
			auto v1 = n1.v;
			auto v2 = n2.v;
			v1 = Num::NonZero(v1);
			v2 = Num::NonZero(v2);
			if (v1.size() == 1 && v2.size() == 1) {
				return Num(v1[0] * v2[0]);
			}
			int maxSize = v1.size() > v2.size() ? v1.size() : v2.size();
			if (maxSize % 3 == 1)
				maxSize += 2;
			else if (maxSize % 3 == 2)
				maxSize += 1;
			Num x0, x1, x2, y0, y1, y2;
			v1 = Karatsuba::AddZero(v1, maxSize - v1.size());
			v2 = Karatsuba::AddZero(v2, maxSize - v2.size());
			x0 = Num(Karatsuba::GetSub(v1, 0, maxSize / 3), true);
			x1 = Num(Karatsuba::GetSub(v1, maxSize / 3, maxSize / 3), true);
			x2 = Num(Karatsuba::GetSub(v1, 2 * maxSize / 3, maxSize / 3), true);
			y0 = Num(Karatsuba::GetSub(v2, 0, maxSize / 3), true);
			y1 = Num(Karatsuba::GetSub(v2, maxSize / 3, maxSize / 3), true);
			y2 = Num(Karatsuba::GetSub(v2, 2 * maxSize / 3, maxSize / 3), true);

			auto Ux = [&](Num t)
			{
				return x2 * t * t + x1 * t + x0;
			};
			auto Uy = [&](Num t)
			{
				return y2 * t * t + y1 * t + y0;
			};
			auto W = [&](Num t) {
				return Ux(t) * Uy(t);
			};
			vector<Num> wi(0);
			for (int i = 0; i < 5; i++)
			{
				wi.push_back(W(Num(i)));

			}
			vector<Num> buffer = wi;
			vector<Num> coeffs(0);
			coeffs.push_back(wi[0]);
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4 - i; j++)
				{
					buffer[j] = (wi[j + 1] - wi[j]) / Num(i + 1);
				}
				wi = buffer;
				coeffs.push_back(buffer[0]);
			}

			vector<Num> buffer1(0);
			for (int i = 0; i < 5; i++)
				buffer1.push_back(coeffs[4]);
			vector<Num> buffer2(0);
			vector<Num> newCoeffs(0);
			newCoeffs.push_back(coeffs[4]);
			for (int i = 0; i < 3; i++)
			{
				buffer2 = vector<Num>(0);
				buffer2.push_back(coeffs[3 - i]);
				for (int j = 0; j < 3 - i; j++)
				{
					buffer2.push_back(buffer2[buffer2.size() - 1] -
						(Num(3 - i - j) * buffer1[j + 1]));
				}
				newCoeffs.push_back(buffer2[buffer2.size() - 1]);
				buffer1 = buffer2;
			}
			newCoeffs.push_back(coeffs[0]);
			/*
			for (int i = 0; i < 5; i++)
			{
				cout << "W[" << i << "] = " << (string)newCoeffs[i] << endl;
			}
			*/
			int shiftSize = maxSize / 3;
			Num X4 = Num(Karatsuba::Shift(newCoeffs[0].v, shiftSize * 4), true);
			Num X3 = Num(Karatsuba::Shift(newCoeffs[1].v, shiftSize * 3), true);
			Num X2 = Num(Karatsuba::Shift(newCoeffs[2].v, shiftSize * 2), true);
			Num X1 = Num(Karatsuba::Shift(newCoeffs[3].v, shiftSize * 1), true);
			Num X0 = Num(newCoeffs[4].v, true);
			return X4 + X3 + X2 + X1 + X0;
			/*
			z2 = Multiply(x1, y1);
			z0 = Multiply(x0, y0);

			z1 = Multiply(Num::SumPosPos(x1, x0), Num::SumPosPos(y1, y0));
			z1 = Num::PositiveSub(z1, z2);
			z1 = Num::PositiveSub(z1, z0);

			result = Shift(z2, maxEven);
			result = Num::SumPosPos(result, Shift(z1, maxEven / 2));
			result = Num::SumPosPos(result, z0);
			*/
			return n1;


		}
	};
	class MillerRabin {
	public:
		static bool Prime(Num n, int rounds = 1) {
			int d;
			int r = 0;
			Num nCopy(n - 1);
			while ((nCopy % 2).v[0] != 1) {
				r++;
				nCopy = nCopy / Num(2);
			}
			d = ToInt(nCopy);
			for (int i = 0; i < rounds; i++)
			{
				int a = Random::Get(2, ToInt(n - 2));
				nCopy = PowMod(Num(a), Num(d), n);
				if (CompareAbs(Num(1).v, nCopy.v) == 0 || CompareAbs(Num(n - 1).v, nCopy.v) == 0)
					continue;
				for (int i = 0; i < ToInt(r - 1); i++)
				{
					nCopy = (nCopy ^ 2) % n;
					if (CompareAbs(Num(n - 1).v, nCopy.v) == 0)
						continue;
				}
				return false;

			}
			return true;
		}
	};
	class Fermat {
	public:
		static bool Prime(Num n, Num a) {
			return CompareAbs(PowMod(a, n - 1, n).v, Num(1).v) == 0;
		}
		static bool Prime(Num n, int rounds = 1) {
			for (int i = 0; i < rounds; i++)
			{
				if (!Prime(n, Num(Random::Get(2, ToInt(n - 1)))))
					return false;
			}
			return true;
		}
	};
	static int ToInt(Num num) {
		int n = 0;
		int ntoAdd = 0;
		for (int i = 0; i < num.v.size(); i++)
		{
			ntoAdd = 1;
			for (int j = 0; j < i; j++)
			{
				ntoAdd *= 10;
			}
			n += ntoAdd * num.v[i];

		}
		return n;
	}
	Num operator *(Num num2) const {
		return Num(NonZero(Karatsuba::Multiply(v, num2.v)), num2.sign == sign);
		//return ToomCook3::Multiply(*this,num2);
	}
	Num operator /(Num num2) const {
		Num n;
		return Division::Divide(*this, num2, n);
	}
	Num operator %(Num num2)const {
		Num n;
		Division::Divide(*this, num2, n);
		return Num(NonZero(n.v), true);
	}
	Num operator ^(Num pow)const {
		return Pow(*this, pow);
	}
	static Num Pow(Num x, Num pow) {
		if (CompareAbs(pow.v, Num(0).v) == 0)
			return Num(1);
		if (CompareAbs(pow.v, Num(1).v) == 0)
			return x;

		if (pow.v[0] % 2 == 0) {
			return Pow(x * x, pow / Num(2));
		}
		else {
			return x * Pow(x * x, pow / Num(2));
		}
	}
	static Num PowMod(Num num, Num pow, Num mod) {
		if (CompareAbs(pow.v, Num(0).v) == 0)
			return 1;
		if (CompareAbs(pow.v, Num(1).v) == 0)
			return num % mod;
		if (pow.v[0] % 2 == 0) {
			return (PowMod(num, pow / 2, mod) ^ 2) % mod;
		}
		else {
			return (num % mod) * (PowMod(num, pow / 2, mod) ^ 2) % mod;
		}
	}
	static const short ZERO_INDEX = (short)'0';
	bool sign;// true -> positive
	vector<short> v;
	Num(string s) {
		if (s[0] == '-') {
			sign = false;
			v = Str2V(s.substr(1));
		}
		else {
			sign = true;
			v = Str2V(s);
		}
	}
	operator string()const {
		string sign = this->sign ? "" : "-";
		string result = "";
		for (int i = v.size() - 1; i >= 0; i--)
		{
			result += Short2Char(v[i]);
		}

		return sign + result;
	}
	Num(vector<short> v, bool sign) {
		this->v = v;
		this->sign = sign;
	}
	Num operator -(Num num2) const {
		return *this + Num(num2.v, !num2.sign);
	}
	Num operator  +(Num num2) const {
		bool local_sign;
		vector<short> value;
		if (sign) {
			if (num2.sign) {
				local_sign = true;
				value = SumPosPos(v, num2.v);
			}
			else {
				value = SumPosNeg(v, num2.v, local_sign);
			}
		}
		else {
			if (num2.sign) {
				value = SumNegPos(v, num2.v, local_sign);
			}
			else {
				value = SumNegNeg(v, num2.v, local_sign);
			}
		}
		return Num(NonZero(value), local_sign);
	}
	static vector<short> SubPosPos(const vector<short>& x1, const  vector<short>& x2,
		bool& sign) {
		if (CompareAbs(x1, x2) != -1) {
			sign = true;
			return PositiveSub(x1, x2);
		}
		else {
			sign = false;
			return PositiveSub(x2, x1);
		}
	}
	static vector<short> SubPosNeg(const vector<short>& x1, const  vector<short>& x2,
		bool& sign) {
		sign = true;
		return SumPosPos(x1, x2);
	}
	static vector<short> SubNegPos(const vector<short>& x1, const  vector<short>& x2,
		bool& sign) {
		return SumNegNeg(x1, x2, sign);
	}
	static vector<short> SubNegNeg(const vector<short>& x1, const  vector<short>& x2,
		bool& sign) {
		return SumNegPos(x1, x2, sign);
	}
	static vector<short>  NonZero(vector<short> v) {
		auto result = vector<short>(1);
		result[0] = v[0];
		int lastNonZero = 0;
		for (size_t i = 0; i < v.size(); i++)
		{
			if (v[i] != 0)
				lastNonZero = i;
		}
		for (size_t i = 1; i <= lastNonZero; i++)
		{
			result.push_back(v[i]);
		}
		return result;
	}
	static int CompareAbs(vector<short> v1, vector<short> v2)
	{
		v1 = NonZero(v1);
		v2 = NonZero(v2);
		if (v1.size() > v2.size())
			return 1;
		else if (v2.size() > v1.size())
			return -1;
		else {
			for (int i = v1.size() - 1; i >= 0; i--)
			{
				if (v1[i] > v2[i])
					return 1;
				else if (v2[i] > v1[i])
					return -1;
			}
			return 0;
		}

	}
	static vector<short> SumNegPos(const vector<short>& x1, const  vector<short>& x2,
		bool& sign) {
		return SumPosNeg(x2, x1, sign);
	}
	static vector<short> SumNegNeg(const vector<short>& x1, const  vector<short>& x2,
		bool& sign) {
		sign = false;
		return SumPosPos(x1, x2);
	}
	static vector<short> SumPosNeg(const vector<short>& x1, const  vector<short>& x2,
		bool& sign) {
		vector<short> result;
		if (CompareAbs(x1, x2) == -1) {
			sign = false;
			result = PositiveSub(x2, x1);
		}
		else {
			sign = true;
			result = PositiveSub(x1, x2);
		}
		return result;
	}
	static vector<short> PositiveSub(const vector<short>& x1, const  vector<short>& x2) {
		if (CompareAbs(x1, x2) == -1) {
			cout << "x1<x2: " << endl;
			exit(1);
		}
		vector<short> result = vector<short>(0);
		vector<short> x1Copy = x1;
		auto X2 = [&](int index) {
			if (index >= x2.size())
				return (short)0;
			return x2[index];
		};
		auto arrange = [&](int index) {
			if (x1Copy[index] < X2(index)) {
				x1Copy[index] += 10;
				while (x1Copy[++index] == 0)
				{
					x1Copy[index] = 9;
				}
				x1Copy[index]--;
			}
		};
		for (int i = 0; i < x1.size(); i++)
		{
			arrange(i);
			result.push_back(x1Copy[i] - X2(i));
		}
		return result;
	}
	static vector<short> SumPosPos(const vector<short>& v1, const vector<short>& v2) {
		short toAdd = 0, currAdd = 0;
		vector<short> result = vector<short>(0);
		auto getAt = [](vector<short> v, int index) {
			if (index >= v.size())
				return (short)0;
			return v[index];
		};
		int maxSize = v1.size() > v2.size() ? v1.size() : v2.size();
		for (int i = 0; i < maxSize; i++)
		{
			currAdd = toAdd + getAt(v1, i) + getAt(v2, i);
			if (currAdd < 10)
				toAdd = 0;
			else
			{
				toAdd = 1;
				currAdd -= 10;
			}
			result.push_back(currAdd);
		}
		if (toAdd == 1)
			result.push_back(1);
		return result;
	}
	Num(int n) :Num(to_string(n)) {}
	Num(long long n) :Num(to_string(n)) {}
	Num(const Num& num) {
		sign = num.sign;
		v = num.v;
	}
	static short Char2Short(char ch) {
		return ch - ZERO_INDEX;
	}
	static char Short2Char(char ch) {
		return (char)((short)ch + ZERO_INDEX);
	}
	static vector<short> Str2V(string  s) {
		int length = s.length();
		auto result = vector<short>(length);
		for (int i = 0; i < length; i++)
		{
			result[length - 1 - i] = Char2Short(s[i]);
		}
		return result;
	}
};


int main()
{
	string a = "1";
	string b = "1";
	Num n1(a);
	Num n2(b);
	//cout << (string)n1 << endl;
	//cout << (string)n2 << endl;
	Num n3 = n1 * n2;
	n3 = Num(Num::Karatsuba::Multiply(n1.v, n2.v), true);
	n3 = Num::ToomCook3::Multiply(n1, n2);

	//cout << (string)n3 << endl;



	cout << Num::Fermat::Prime(1235477, 10) << endl;
	cout << Num::MillerRabin::Prime(225325, 10) << endl;
	//std::cout << "Hello World!\n"; 
	//cout <<(string) Num::PowMod(Num (24), Num (220), Num (221));
	/*
	vector<Num> v(0);
	for (int i = 1; i < 4; i++)
	{
		v.push_back(Num(i*11));
	}
	*/
	// reference copy testing - passed
	/*
	cout << (string)v[0] << endl;
	vector<Num> v2 = v;
	cout << (string)v2[0] << endl;
	v2[0].v[0] = 0;
	cout << (string)v[0]<<endl;
	cout << (string)v2[0] << endl;
	*/
}